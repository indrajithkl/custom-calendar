# Custom Calendar

![Custom Calendar](docs/images/customCalendar.PNG "Custom Calendar")


Custom Calendar is an HTML based customizable calendar build from scratch. Its similar(inspiration) to [Full Calendar](http://fullcalendar.io)

### Features

 - Multiple Months in one View
 - Fully Customizable with CSS
 - Support custom redraw methods
 - Events (appointments, birthdays...)
 - Easy to integrate with existing applications


### Version 
1.0 Alpha

### Dependencies

Custom Calendar uses some of opensource JS libraries for rendering

* [JQuery](http://jquery.com) - For rendering and DOM manipulation
* [Moment.js](http://momentjs.com/) - For date calculations
* [Hint.css](https://github.com/chinchang/hint.css) - Kool Tooltips!

### Installation

You need to first include all the dependencies (*see above. also you can find those in deps folder in this repo*) in your html file and include the customCalendar.js after that.

Link the css comes with **Custom Calendar**

To render the calendar you need to have a **div** container.  For example say

```html
<div id="calendar"></div>
```

Attach the below css to the container for rendering calendar as it should be.

```css
#calendar {
    display: inline-block;
}
```

For integration and explanation refer the [demo code](demo.html)

### Future Features
* Event Deletion.
* Events UI Customization - (you can drag and drop the events to any day to change the dates).
* Better control over month display.