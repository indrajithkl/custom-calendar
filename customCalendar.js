// Initialization Class
// @dom : JQuery DOM Object - The container for the calendar
function CustomCalendarv2(dom) {
    this.dom = dom;
    this.currentDate = new moment();
    this.rows = 6;
    this.cols = 7;
    this.seperator = "-";
    this.calendarContainer = $("<div id='custom-calendar'></div>");
    this.dateHeader = $("<span class='calendarREV2Header' id='date-header'></span>");
    this.buttonsContainer = $(' <span id="buttons-container">'+
                                '<button id="previousMonth" class="custom-calendar-btn previous"></button>'+
                                '<button id="today" class="custom-calendar-btn today">Today</button>'+
                                '<button id="nextMonth" class="custom-calendar-btn next"></button>'+
                        '</span>'
                        );

    this.table = $("<table class='calendarREV2Table'></table>");

    this.thead = $("<thead>"+
                        "<tr class='day-headers'>"+
                            "<th>Sun</th>"+
                            "<th>Mon</th>"+
                            "<th>Tue</th>"+
                            "<th>Wed</th>"+
                            "<th>Thu</th>"+
                            "<th>Fri</th>"+
                            "<th>Sat</th>"+
                        "</tr>"+
                    "</thead>");
    this.tbody = $("<tbody></tbody>");

    this.redrawBindMethod = null;
    this.redrawparamMethod = null;
    this.events = [];
    this.renderCalendarSkeleton();
};

CustomCalendarv2.prototype.renderCalendarSkeleton = function(){
   
    var title = $("<h1></h1>");

    this.buttonsContainer.appendTo(this.calendarContainer);
   
    this.dateHeader.appendTo(this.calendarContainer);

    this.thead.appendTo(this.table);
    this.tbody.appendTo(this.table);
        
    this.table.addClass('nospacing');
    this.table.appendTo(this.calendarContainer);
    this.calendarContainer.appendTo(this.dom);
    this.calendarContainer.css({
        'padding':'5px'
    })
    this.drawCalendar(new moment());
}

// @startDate - moment Object
CustomCalendarv2.prototype.drawCalendar = function(startingDate){
    
    this.currentDate = startingDate != null ? startingDate : new moment();
    var endDate = this.currentDate;
    var startDate = this.currentDate;
    var startingDay = moment(this.currentDate).isoWeekday()==7 ? this.currentDate :  moment(this.currentDate).subtract(moment(this.currentDate).isoWeekday(),'days');
    startDate = startingDay;


    this.tbody.empty();

    for(var i = 0; i < this.rows; i++){
        var tr = $("<tr></tr>");
        for(var j = 0; j < this.cols; j++){
            var dayCell =  $("<td data-date="+moment(startingDay).format("YYYY-MM-DD")+"><span class='cc-day-number' data-date="+moment(startingDay).format("YYYY-MM-DD")+">"+moment(startingDay).date()+"</span></td>");
            var monthShortName =  moment(startingDay).format('MMM');
            var cellMonthIndicator = null;
            cellMonthIndicator = $("<span>"+monthShortName+"</span>").css({
                'color': 'black',
                'font-size': 'large',
                'position': 'absolute',
                'padding-left': '0%',
                'opacity': '0.08',
                'font-weight': 'bolder', 
                'top': '40%',
                'left':' 20%'});
            
            if(moment(startingDay).date()==1){
                dayCell.css({
                    background:'rgba(229, 246, 255, 0.58)',
                    'font-weight':'bold'
                });
            }
            if(moment(moment().format('MMMM DD, YYYY')).isSame(startingDay.format('MMMM DD, YYYY'))){
                dayCell.css({
                    'background':'#fdf9d4'
                });
            }

            if(moment(startingDay.format('MMMM DD, YYYY')).diff(moment().format('MMMM DD, YYYY')) < -1){
                dayCell.addClass('cc-past-day');
            }

            dayCell.addClass("cc-day");
            cellMonthIndicator.attr('id',"monthLabel");
            cellMonthIndicator.appendTo(dayCell);
            dayCell.appendTo(tr);
            endDate = startingDay;
            startingDay = moment(startingDay).add(1,'days');
        }
        tr.appendTo(this.tbody);
    }

    this.renderHeader(startDate, endDate);
    // this.table.empty(); // Clear already rendered Table

    // thead.appendTo(this.table);
    // tbody.appendTo(this.table);
    


    // table.appendTo(calendarContainer);
    // calendarContainer.appendTo($(this.dom));

    this.setupUIEvents();
    if(this.events.length>0){
        this.drawEvents();
    }
    
}

// @configObj - JSON Object
// sample params
// {
//    rows: 8,
//    cols: 7
// }
// redrawBindMethod sample
// 
CustomCalendarv2.prototype.config = function(configObj){
    if(configObj.rows){
        this.rows = parseInt(configObj.rows);
    }
    // if(configObj.cols && !(parseInt(configObj.cols) !== parseInt(configObj.cols))){
    //     this.cols = configObj.cols;
    // }

    if(configObj.seperator){
        this.seperator = configObj.seperator;
    }

    if(configObj.redrawBindMethod){
        this.redrawBindMethod = configObj.redrawBindMethod.fn;
        this.redrawparamMethod = configObj.redrawBindMethod.paramsmethod;
    }
}

// Redering Date Headers
// @startDate : moment date object - Start Date to display on the header
// @endDate   : moment date object - End Date to display on the header
CustomCalendarv2.prototype.renderHeader = function(startDate, endDate){
    this.dateHeader.empty();
    this.dateHeader.append(startDate.format("MMM DD") +" "+this.seperator+" "+ endDate.format("MMM DD")+", "+endDate.year());
}

// Used to setting up events for buttons and Cells inside the calendar
CustomCalendarv2.prototype.setupUIEvents = function(){
    var self = this;

    $('#nextMonth').off().on('click', function(){
        self.nextMonth();
        if(self.redrawBindMethod){
            self.redrawBindMethod.apply(self, self.redrawparamMethod());
        }
    });

    $('#previousMonth').off().on('click', function(){
        self.previousMonth();
        if(self.redrawBindMethod){
            self.redrawBindMethod.apply(self, self.redrawparamMethod());
        }
    });

    $('#today').off().on('click', function(){
        self.today();
        if(self.redrawBindMethod){
            self.redrawBindMethod.apply(self, self.redrawparamMethod());
        }
    });
}

// Clear Items in the calendar / Re-Render the calendar
// @startDate : moment object - If specified will be used as 
//                              start date of the calendar after
//                              resetting the calendar
CustomCalendarv2.prototype.clearCalendar = function(startDate){
    if(startDate){
        this.currentDate = startDate;
    }
   this.drawCalendar(this.currentDate);
}

// Navigates to today's date
CustomCalendarv2.prototype.today = function(){
    this.currentDate = new moment();
    this.drawCalendar(this.currentDate);
}

// Navigates to previous month from the current calendar date 
CustomCalendarv2.prototype.previousMonth = function(){
    this.currentDate = moment(this.currentDate).subtract(1,'months');
    this.drawCalendar(this.currentDate);
}

// Navigates to next month from the current calendar date
CustomCalendarv2.prototype.nextMonth = function(){
    this.currentDate = moment(this.currentDate).add(1,'months');
    this.drawCalendar(this.currentDate);
}

// Navigate to specified date
// @date : moment object
CustomCalendarv2.prototype.gotoDate = function(date){
    this.currentDate = moment(date);
    this.drawCalendar(this.currentDate);
}

// Can be used to add items to the Calendar
CustomCalendarv2.prototype.addAndRefresh = function(){

}

// Adds/Attach an event to specified range of dates
// @eventObject : JSON Object
CustomCalendarv2.prototype.addEvent = function(eventObject){
    this.events.push(eventObject);    
    this.drawEvents();
}

// Draws/Re-draws already attached Events - Not ui events (If you know what I mean)
CustomCalendarv2.prototype.drawEvents = function(){
    var self = this;
    
    for(var i in this.events){
        var curEvntStartDate = self.events[i]["start-date"];
        var curEvntEndDate = self.events[i]["end-date"];
        var numDays = moment(curEvntEndDate).diff(curEvntStartDate,'days');
        for(var j = 0; j<=numDays; j++){
            $('span[data-date="'+
                moment(curEvntStartDate)
                .add(j,'days')
                .format('YYYY-MM-DD')+'"]')
                .addClass('cc-event-day')
                .addClass('hint--bottom')
                .addClass('hint--medium')
                .css('background-color',self.events[i]['color'])
                .css('color','white')
                .attr('aria-label',self.events[i]['event-title']);
        }
    }
}